package com.myvideogallery.app.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;

import javax.inject.Inject;

/**
 * Created by Rory_McCormack on 3/08/2016.
 */
public class Utils {

    public static String getRealPathFromVideoUri(Context context, Uri uri) {
        String result;
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public static long getVideoDuration(Context context, Uri uri) {
        long result = -1;
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = MediaStore.Video.query(cr, uri, new String[]{MediaStore.Video.VideoColumns.DURATION});
        if(cursor.moveToFirst()) {
            String duration = cursor.getString(0);
            result = Long.valueOf(duration);
        }

        return result;
    }

    public static Drawable getThumbnail(Context context, String path) {
        Bitmap bmp = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
        return new BitmapDrawable(context.getResources(), bmp);
    }
}
