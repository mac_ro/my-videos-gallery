package com.myvideogallery.app.ui;

import android.support.annotation.StringRes;

/**
 * Created by Rory_McCormack on 2/08/2016.
 */
public interface BaseView {

    void displayError(@StringRes int stringId);
}
