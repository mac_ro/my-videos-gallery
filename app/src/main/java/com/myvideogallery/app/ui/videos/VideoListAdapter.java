package com.myvideogallery.app.ui.videos;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.myvideogallery.R;
import com.myvideogallery.app.utils.Utils;
import com.myvideogallery.domain.model.Video;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Rory_McCormack on 2/08/2016.
 */
public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.VideoHolder> {

    private final MyVideosPresenter.View videoListView;
    private final Context context;
    private List<Video> videoList;

    public VideoListAdapter(Context context, MyVideosPresenter.View videoListView) {
        this.videoListView = videoListView;
        this.context = context;
        this.videoList = new ArrayList<>();
    }

    @Override
    public VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.video_list_item, parent, false);
        return new VideoHolder(v, videoListView);
    }

    @Override
    public void onBindViewHolder(VideoHolder holder, int position) {
        Video video = videoList.get(position);
        holder.bind(video);
        Locale locale = Locale.getDefault();
        holder.timestamp.setText(video.getTimestampString(locale));
        holder.duration.setText(video.getDurationString(locale));
        holder.runningTime.setText(video.getDeviceRunningString(locale));
        holder.thumbnail.setImageDrawable(Utils.getThumbnail(context,video.getPath()));
    }

    @Override
    public int getItemCount() {
        return videoList != null ? videoList.size() : 0;
    }

    public void addNewItem(Video video) {
        videoList.add(video);
        notifyItemInserted(videoList.size());
    }

    public void addItems(List<Video> videos) {
        videoList.addAll(videos);
        notifyDataSetChanged();
    }


    public void removeDeletedItem(int position) {
        videoList.remove(position);
        notifyItemRemoved(position);
    }

    static class VideoHolder extends RecyclerView.ViewHolder {

        private MyVideosPresenter.View videoListView;
        private Video video;

        @Bind(R.id.video_thumbnail) ImageView thumbnail;
        @Bind(R.id.video_timestamp) TextView timestamp;
        @Bind(R.id.video_device_running_time) TextView runningTime;
        @Bind(R.id.video_duration) TextView duration;

        public VideoHolder(View itemView, MyVideosPresenter.View videoListView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.videoListView = videoListView;
        }

        public void bind(Video video) {
            this.video = video;
        }

        @OnClick(R.id.video_content_frame)
        public void videoContentClicked() {
            videoListView.goToWatchVideo(video);
        }

        @OnClick(R.id.video_delete)
        public void deleteClicked() {
            int position = getAdapterPosition();
            videoListView.deleteVideoClicked(video, position);
        }

        @OnClick(R.id.video_share)
        public void shareClicked() {
            videoListView.shareVideoClicked(video);
        }

    }
}
