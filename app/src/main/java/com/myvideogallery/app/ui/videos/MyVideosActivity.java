package com.myvideogallery.app.ui.videos;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer.util.Util;
import com.myvideogallery.R;
import com.myvideogallery.app.ui.BaseActivity;
import com.myvideogallery.app.ui.player.PlayerActivity;
import com.myvideogallery.app.utils.Utils;
import com.myvideogallery.domain.model.Video;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.OnClick;

public class MyVideosActivity extends BaseActivity<MyVideosPresenter> implements MyVideosPresenter.View {

    private static final int VIDEO_REQUEST = 100;
    private static final int VIDEO_CAPTURE_PERMISSION = 200;
    private static final int STORAGE_PERMISSION = 300;

    private VideoListAdapter adapter;

    @Bind(R.id.my_vids_toolbar) Toolbar toolbar;
    @Bind(R.id.videos_recycler_view) RecyclerView recyclerView;
    @Bind(R.id.empty_list_message) TextView emptyMessage;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_vids;
    }

    @Override
    protected void injectDependencies() {
        getApplicationComponent().inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        adapter = new VideoListAdapter(getApplicationContext(), this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayout.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        if (checkStoragePermissions(STORAGE_PERMISSION,
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            getPresenter().getAllVideos();
        }

    }

    @TargetApi(23)
    private boolean checkCameraPermission() {
        if (requiresPermission(Manifest.permission.CAMERA)) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, VIDEO_CAPTURE_PERMISSION);
            return false;
        }

        return true;
    }

    @TargetApi(23)
    private boolean requiresPermission(String permission) {
        return Util.SDK_INT >= 23
                && ContextCompat.checkSelfPermission(this, permission)
                != PackageManager.PERMISSION_GRANTED;
    }

    @TargetApi(23)
    private boolean checkStoragePermissions(int request, String... permissions) {
        List<String> requestList = new ArrayList<>();
        for (String permission : permissions) {
            if (requiresPermission(permission)) {
                requestList.add(permission);
            }
        }

        if (requestList.isEmpty()) {
            return true;
        }

        String[] array = requestList.toArray(new String[requestList.size()]);
        ActivityCompat.requestPermissions(this, array, request);
        return false;
    }

    @OnClick(R.id.fab)
    public void onCameraClicked() {
        if (checkCameraPermission()) {
            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 20);
            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
            startActivityForResult(intent, VIDEO_REQUEST);
        }
    }

    @Override
    public void goToWatchVideo(Video video) {
        if (checkCameraPermission()) {
            startActivity(PlayerActivity.getIntent(this, video.getPath()));
        }
    }

    @Override
    public void renderVideoList(List<Video> videos) {
        adapter.addItems(videos);
    }

    @Override
    public void displayEmptyListMessage() {
        emptyMessage.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyListMessage() {
        emptyMessage.setVisibility(View.GONE);
    }

    @Override
    public void shareVideoClicked(Video video) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(video.getPath()));
        shareIntent.setType("video/mp4");
        startActivity(Intent.createChooser(shareIntent, "Send video via:"));
    }

    @Override
    public void deleteVideoClicked(final Video video, final int position) {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.delete_file_prompt))
                .setMessage(getString(R.string.delete_file_msg))
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getPresenter().deleteVideo(video, position);
                    }
                })
                .create()
                .show();
    }

    @Override
    public void removeDeletedVideoFromList(int position) {
        adapter.removeDeletedItem(position);
    }

    @Override
    public void addCreatedVideoToList(Video video) {
        adapter.addNewItem(video);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length <= 0) {
            return;
        }

        switch (requestCode) {
            case STORAGE_PERMISSION:
                boolean granted = true;

                for (int i : grantResults) {
                    if (i != PackageManager.PERMISSION_GRANTED)
                        granted = false;
                }

                if (!granted) {
                    Toast.makeText(this, R.string.read_permission_denied, Toast.LENGTH_LONG).show();
                    return;
                }

                getPresenter().getAllVideos();
                break;

            case VIDEO_CAPTURE_PERMISSION:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, R.string.camera_permission_denied, Toast.LENGTH_LONG).show();
                    return;
                }

                onCameraClicked();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VIDEO_REQUEST && resultCode == RESULT_OK) {

            Uri uri = data.getData();
            String path = Utils.getRealPathFromVideoUri(this, uri);
            long duration = Utils.getVideoDuration(this, uri);
            long timestamp = Calendar.getInstance(Locale.getDefault()).getTimeInMillis();
            long deviceRunningTime = SystemClock.elapsedRealtime();

            getPresenter().createVideo(path, duration, timestamp, deviceRunningTime);
        }
    }

}
