package com.myvideogallery.app.ui.videos;


import com.myvideogallery.R;
import com.myvideogallery.app.ui.BaseView;
import com.myvideogallery.app.ui.Presenter;
import com.myvideogallery.domain.model.Video;
import com.myvideogallery.domain.usecase.CreateVideo;
import com.myvideogallery.domain.usecase.DeleteVideo;
import com.myvideogallery.domain.usecase.GetAllVideos;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Rory_McCormack on 2/08/2016.
 */
public class MyVideosPresenter extends Presenter<MyVideosPresenter.View>
        implements GetAllVideos.CallBack, CreateVideo.CallBack, DeleteVideo.CallBack {

    public interface View extends BaseView {

        void renderVideoList(List<Video> videos);

        void displayEmptyListMessage();

        void hideEmptyListMessage();

        void goToWatchVideo(Video video);

        void shareVideoClicked(Video video);

        void deleteVideoClicked(Video video, int position);

        void addCreatedVideoToList(Video video);

        void removeDeletedVideoFromList(int position);
    }

    private final GetAllVideos getAllVideos;
    private final CreateVideo createVideo;
    private final DeleteVideo deleteVideo;

    private int deleteVideoPosition;

    @Inject
    public MyVideosPresenter(GetAllVideos getAllVideos, CreateVideo createVideo, DeleteVideo deleteVideo) {
        super(getAllVideos, createVideo, deleteVideo);
        this.getAllVideos = getAllVideos;
        this.createVideo = createVideo;
        this.deleteVideo = deleteVideo;
    }

    public void getAllVideos() {
        getAllVideos.execute(this);
    }

    public void createVideo(String path, long duration, long timestamp, long runningTime) {
        createVideo.execute(this, path, duration, timestamp, runningTime);
    }

    public void deleteVideo(Video video, int position) {
        deleteVideoPosition = position;
        deleteVideo.execute(this, video.getId());
    }

    @Override
    public void onVideosRetrieved(List<Video> videos) {
        if (getView() != null) {
            getView().hideEmptyListMessage();
            getView().renderVideoList(videos);
        }
    }

    @Override
    public void onNoVideosRetrieved() {
        if (getView() != null) {
            getView().displayEmptyListMessage();
        }
    }

    @Override
    public void onGetVideosError() {
        if (getView() != null) {
            onError();
        }
    }

    @Override
    public void onVideoFileCreated(Video video) {
        if (getView() != null) {
            getView().hideEmptyListMessage();
            getView().addCreatedVideoToList(video);
        }
    }

    @Override
    public void onCreateVideoError() {
        if (getView() != null) {
            getView().displayError(R.string.error_generic);
        }
    }

    @Override
    public void onVideoDeleted() {
        if (getView() != null) {
            getView().removeDeletedVideoFromList(deleteVideoPosition);
        }
    }
}
