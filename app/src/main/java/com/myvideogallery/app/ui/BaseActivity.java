package com.myvideogallery.app.ui;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.myvideogallery.app.ApplicationComponent;
import com.myvideogallery.app.MyVideoGalleryApp;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by Rory_McCormack on 2/08/2016.
 */
public abstract class BaseActivity<P extends Presenter> extends AppCompatActivity implements BaseView {

    private P presenter;

    @LayoutRes
    protected abstract int getLayoutId();

    protected abstract void injectDependencies();

    @Inject
    protected void setPresenter(P presenter) {
        this.presenter = presenter;
    }

    public P getPresenter() {
        return presenter;
    }

    public ApplicationComponent getApplicationComponent() {
        return MyVideoGalleryApp.get(this).getComponent();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        injectDependencies();
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void displayError(@StringRes int stringId) {
        Toast.makeText(this, stringId, Toast.LENGTH_LONG).show();
    }
}
