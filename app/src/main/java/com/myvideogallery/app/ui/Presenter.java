package com.myvideogallery.app.ui;

import android.support.annotation.Nullable;

import com.myvideogallery.R;
import com.myvideogallery.domain.UseCase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


/**
 * Created by Rory_McCormack on 2/08/2016.
 */
public abstract class Presenter<V extends BaseView> implements UseCase.BaseCallBack{

    @Nullable
    private V view;

    private List<UseCase> useCases;

    protected Presenter(UseCase... useCases) {
        this.useCases = convertToList(useCases);
    }

    private List<UseCase> convertToList(UseCase... useCases) {
        if (useCases == null) {
            throw new NullPointerException("UseCases must not be null");
        }

        ArrayList<UseCase> list = new ArrayList<>(useCases.length);
        Collections.addAll(list, useCases);
        return list;
    }

    @Nullable
    public V getView() {
        return view;
    }

    public void attachView(V view) {
        this.view = view;
    }

    public void detachView() {
        this.view = null;
        for (UseCase useCase : useCases) {
            useCase.unsubscribeAll();
        }

    }

    @Override
    public void onError() {
        if (view != null) {
            view.displayError(R.string.error_generic);
        }
    }

}
