package com.myvideogallery.app.ui.player;


import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaCodec;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import com.google.android.exoplayer.AspectRatioFrameLayout;
import com.google.android.exoplayer.ExoPlaybackException;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaCodecSelector;
import com.google.android.exoplayer.MediaCodecTrackRenderer;
import com.google.android.exoplayer.MediaCodecVideoTrackRenderer;
import com.google.android.exoplayer.SampleSource;
import com.google.android.exoplayer.TrackRenderer;
import com.google.android.exoplayer.extractor.ExtractorSampleSource;
import com.google.android.exoplayer.upstream.Allocator;
import com.google.android.exoplayer.upstream.DataSource;
import com.google.android.exoplayer.upstream.DefaultAllocator;
import com.google.android.exoplayer.upstream.DefaultUriDataSource;
import com.google.android.exoplayer.util.Util;
import com.myvideogallery.R;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Rory_McCormack on 4/08/2016.
 */
public class PlayerActivity extends AppCompatActivity
        implements SurfaceHolder.Callback, ExoPlayer.Listener, MediaCodecVideoTrackRenderer.EventListener {

    private static final String TAG = "PlayerActivity";
    private static final String PATH = "path";

    private static final int BUFFER_SEGMENT_SIZE = 64 * 1024;
    private static final int BUFFER_SEGMENT_COUNT = 200;

    private TrackRenderer videoRenderer;
    private ExoPlayer exoPlayer;

    @Bind(R.id.video_player_frame) AspectRatioFrameLayout videoFrame;
    @Bind(R.id.video_surface_view) SurfaceView surfaceView;

    public static Intent getIntent(Context context, String path) {
        Bundle bundle = new Bundle();
        bundle.putString(PATH, path);
        Intent intent = new Intent(context, PlayerActivity.class);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_video_player);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        String path = bundle.getString(PATH);

        if (path == null) {
            Toast.makeText(getApplicationContext(), R.string.player_error_message, Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        File file = new File(path);
        Uri uri = Uri.fromFile(file);

        if (Util.SDK_INT >= 17) {
            checkRotation(uri);
        }

        surfaceView.getHolder().addCallback(this);

        Allocator allocator = new DefaultAllocator(BUFFER_SEGMENT_SIZE);
        DataSource dataSource = new DefaultUriDataSource(this, null, "userAgent");
        SampleSource sampleSource = new ExtractorSampleSource(uri, dataSource, allocator,
                BUFFER_SEGMENT_COUNT * BUFFER_SEGMENT_SIZE);
        Handler handler = new Handler();
        videoRenderer = new MediaCodecVideoTrackRenderer(this, sampleSource, MediaCodecSelector.DEFAULT,
                MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT, 3000, handler, this, 5000);

        exoPlayer = ExoPlayer.Factory.newInstance(1);
        exoPlayer.addListener(this);
        exoPlayer.prepare(videoRenderer);
    }

    private void checkRotation(Uri uri) {
        MediaMetadataRetriever mr = new MediaMetadataRetriever();
        mr.setDataSource(this, uri);

        String rotation = mr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
        Log.d(TAG, "Rotation " + rotation);

        if (rotation.matches("0") || rotation.matches("180")) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        exoPlayer.setPlayWhenReady(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        exoPlayer.setPlayWhenReady(false);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        exoPlayer.release();
        exoPlayer = null;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (exoPlayer != null && videoRenderer != null) {
            exoPlayer.sendMessage(videoRenderer, MediaCodecVideoTrackRenderer.MSG_SET_SURFACE, holder.getSurface());
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // Do nothing.
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        surfaceView = null;
        if (exoPlayer != null && videoRenderer != null) {
            exoPlayer.blockingSendMessage(
                    videoRenderer, MediaCodecVideoTrackRenderer.MSG_SET_SURFACE, surfaceView);
        }
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == ExoPlayer.STATE_ENDED) {
            exoPlayer.seekTo(0);
        }
    }

    @Override
    public void onPlayWhenReadyCommitted() {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        Log.e("ExoPlayer", error.getMessage());
    }

    @Override
    public void onDroppedFrames(int count, long elapsed) {

    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
        videoFrame.setAspectRatio(height == 0 ? 1 : (width * pixelWidthHeightRatio) / height);
    }

    @Override
    public void onDrawnToSurface(Surface surface) {

    }

    @Override
    public void onDecoderInitializationError(MediaCodecTrackRenderer.DecoderInitializationException e) {

    }

    @Override
    public void onCryptoError(MediaCodec.CryptoException e) {

    }

    @Override
    public void onDecoderInitialized(String decoderName, long elapsedRealtimeMs, long initializationDurationMs) {

    }
}
