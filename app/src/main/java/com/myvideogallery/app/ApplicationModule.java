package com.myvideogallery.app;

import android.content.Context;

import com.myvideogallery.app.thread.IOThread;
import com.myvideogallery.app.thread.MainThread;
import com.myvideogallery.domain.thread.PostExecutionThread;
import com.myvideogallery.domain.thread.SubscriptionThreadIO;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Rory_McCormack on 3/08/2016.
 */
@Module
public class ApplicationModule {

    private Context context;

    public ApplicationModule(Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    Context provideContext() {
        return context;
    }

    @Provides
    @Singleton
    SubscriptionThreadIO provideIOThread() {
        return new IOThread();
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread() {
        return new MainThread();
    }
}
