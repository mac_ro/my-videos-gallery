package com.myvideogallery.app;

import com.myvideogallery.app.ui.videos.MyVideosActivity;
import com.myvideogallery.data.DataModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Rory_McCormack on 3/08/2016.
 */
@Singleton
@Component(modules = {ApplicationModule.class, DataModule.class})
public interface ApplicationComponent {

    void inject(MyVideosActivity activity);
}
