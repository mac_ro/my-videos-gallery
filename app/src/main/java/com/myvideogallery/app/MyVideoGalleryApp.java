package com.myvideogallery.app;

import android.app.Application;
import android.content.Context;

import com.myvideogallery.data.DataModule;

/**
 * Created by Rory_McCormack on 3/08/2016.
 */
public class MyVideoGalleryApp extends Application {

    private ApplicationComponent component;

    public static MyVideoGalleryApp get(Context context) {
        return (MyVideoGalleryApp) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initDependencyInjection();
    }

    private void initDependencyInjection() {
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .dataModule(new DataModule())
                .build();

    }

    public ApplicationComponent getComponent() {
        return component;
    }
}
