package com.myvideogallery.app.thread;

import com.myvideogallery.domain.thread.PostExecutionThread;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Rory_McCormack on 2/08/2016.
 */
public class MainThread implements PostExecutionThread {

    @Override
    public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
