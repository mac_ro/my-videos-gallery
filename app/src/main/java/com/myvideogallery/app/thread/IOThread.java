package com.myvideogallery.app.thread;

import com.myvideogallery.domain.thread.SubscriptionThreadIO;

import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by Rory_McCormack on 2/08/2016.
 */
public class IOThread implements SubscriptionThreadIO {

    @Override
    public Scheduler getScheduler() {
        return Schedulers.io();
    }
}
