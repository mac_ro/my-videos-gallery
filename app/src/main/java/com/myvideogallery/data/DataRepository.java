package com.myvideogallery.data;

import com.myvideogallery.domain.Repository;
import com.myvideogallery.domain.model.Video;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Rory_McCormack on 2/08/2016.
 */
public class DataRepository implements Repository {

    private final FileManager fileManager;
    private final VideoSerializer serializer;

    @Inject
    public DataRepository(FileManager fileManager, VideoSerializer serializer) {
        this.fileManager = fileManager;
        this.serializer = serializer;
    }

    @Override
    public Observable<List<Video>> getAllVideos() {
        File dir = fileManager.getVideosDir();
        if (dir == null) {
            return Observable.error(new Exception("Files cannot be accessed"));
        }

        List<Video> videos = new ArrayList<>();
        for (File file : dir.listFiles()) {
            String content = fileManager.readFileContent(file);
            Video video = serializer.deserialize(content);
            videos.add(video);
        }

        return Observable.just(videos);
    }

    @Override
    public Observable<Video> createVideoFile(Video video) {
        String id = video.getId();
        String content = serializer.serialize(video);
        File file = fileManager.createFile(id);
        fileManager.writeToFile(file, content);

        return Observable.just(video);
    }

    @Override
    public Observable<Boolean> deleteVideoFile(String id) {
        return Observable.just(fileManager.deleteFile(id));
    }

}
