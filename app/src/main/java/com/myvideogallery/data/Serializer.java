package com.myvideogallery.data;

/**
 * Created by Rory_McCormack on 3/08/2016.
 */
public interface Serializer<T> {

    T deserialize(String json);

    String serialize(T t);
}
