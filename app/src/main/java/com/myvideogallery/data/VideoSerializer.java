package com.myvideogallery.data;

import com.google.gson.Gson;
import com.myvideogallery.domain.model.Video;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Rory_McCormack on 3/08/2016.
 */
@Singleton
public class VideoSerializer implements Serializer<Video> {

    private final Gson gson;

    @Inject
    public VideoSerializer(Gson gson) {
        this.gson = gson;
    }

    @Override
    public Video deserialize(String json) {
        return gson.fromJson(json, Video.class);
    }

    @Override
    public String serialize(Video video) {
        return gson.toJson(video);
    }
}
