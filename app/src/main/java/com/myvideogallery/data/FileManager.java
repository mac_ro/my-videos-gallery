package com.myvideogallery.data;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.inject.Inject;

/**
 * Created by Rory_McCormack on 2/08/2016.
 */
public class FileManager {

    private File directory;

    @Inject
    public FileManager(Context context) {
        this.directory = context.getFilesDir();
    }

    public File getVideosDir() {
        String path = new StringBuilder()
                .append(directory)
                .append(File.separator)
                .append("VID")
                .toString();

        File dir = new File(path);

        if (!dir.exists()) {
            if (!dir.mkdir()) {
                return null;
            }
        }

        return dir;
    }

    public File createFile(String id) {
        if (getVideosDir() == null) {
            return null;
        }

        return new File(getVideosDir(), id);
    }

    public void writeToFile(File file, String fileContent) {
        if (!file.exists()) {
            try {
                FileWriter writer = new FileWriter(file);
                writer.write(fileContent);
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String readFileContent(File file) {
        StringBuilder fileContentBuilder = new StringBuilder();
        if (file.exists()) {
            String stringLine;
            try {
                FileReader fileReader = new FileReader(file);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                while ((stringLine = bufferedReader.readLine()) != null) {
                    fileContentBuilder.append(stringLine + "\n");
                }
                bufferedReader.close();
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return fileContentBuilder.toString();
    }

    public boolean deleteFile(String id) {
        File file = createFile(id);
        return file.delete();
    }
}
