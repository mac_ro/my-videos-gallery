package com.myvideogallery.data;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myvideogallery.domain.Repository;

import javax.inject.Singleton;

import auto.parcelgson.gson.AutoParcelGsonTypeAdapterFactory;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Rory_McCormack on 3/08/2016.
 */
@Module
public class DataModule {

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .registerTypeAdapterFactory(new AutoParcelGsonTypeAdapterFactory())
                .create();
    }

    @Provides
    @Singleton
    VideoSerializer provideSerializer(Gson gson) {
        return new VideoSerializer(gson);
    }

    @Provides
    @Singleton
    FileManager provideFileManager(Context context) {
        return new FileManager(context);
    }

    @Provides
    @Singleton
    Repository provideRepository(FileManager fileManager, VideoSerializer serializer) {
        return new DataRepository(fileManager, serializer);
    }
}
