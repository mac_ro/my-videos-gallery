package com.myvideogallery.domain;

import com.myvideogallery.domain.thread.PostExecutionThread;
import com.myvideogallery.domain.thread.SubscriptionThreadIO;

import javax.inject.Inject;

import rx.Observable;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Rory_McCormack on 2/08/2016.
 */
public abstract class UseCase {

    public interface BaseCallBack {
        void onError();
    }

    private CompositeSubscription subscriptions = new CompositeSubscription();
    private SubscriptionThreadIO subscribeOnThread;
    private PostExecutionThread observeOnThread;

    @Inject
    public void setSubscribeOnThread(SubscriptionThreadIO subscribeOnThread) {
        this.subscribeOnThread = subscribeOnThread;
    }

    @Inject
    public void setObserveOnThread(PostExecutionThread observeOnThread) {
        this.observeOnThread = observeOnThread;
    }

    protected <T> void subscribeOnIO(Observable<T> observable, DefaultSubscriber<T> subscriber) {
        subscriptions.add(observable
                .observeOn(observeOnThread.getScheduler())
                .subscribeOn(subscribeOnThread.getScheduler())
                .subscribe(subscriber));
    }

    public void unsubscribeAll() {
        if (subscriptions.hasSubscriptions()) {
            subscriptions.unsubscribe();
        }
    }

}
