package com.myvideogallery.domain;

import com.myvideogallery.domain.model.Video;

import java.util.List;

import rx.Observable;

/**
 * Created by Rory_McCormack on 2/08/2016.
 */
public interface Repository {

    Observable<Video> createVideoFile(Video video);

    Observable<Boolean> deleteVideoFile(String id);

    Observable<List<Video>> getAllVideos();
}
