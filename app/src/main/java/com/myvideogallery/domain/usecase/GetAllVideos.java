package com.myvideogallery.domain.usecase;

import com.myvideogallery.domain.DefaultSubscriber;
import com.myvideogallery.domain.Repository;
import com.myvideogallery.domain.UseCase;
import com.myvideogallery.domain.model.Video;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Rory_McCormack on 2/08/2016.
 */
public class GetAllVideos extends UseCase {

    public interface CallBack extends BaseCallBack {

        void onVideosRetrieved(List<Video> videos);

        void onNoVideosRetrieved();

        void onGetVideosError();
    }

    private final Repository repository;

    @Inject
    public GetAllVideos(Repository repository) {
        this.repository = repository;
    }

    public void execute(final CallBack callBack) {
        Observable<List<Video>> observable = repository.getAllVideos();

        subscribeOnIO(observable, new DefaultSubscriber<List<Video>>(callBack) {
            @Override
            public void onError(Throwable e) {
                callBack.onGetVideosError();
            }

            @Override
            public void onNext(List<Video> videos) {
                super.onNext(videos);

                if (videos.isEmpty()) {
                    callBack.onNoVideosRetrieved();
                    return;
                }

                callBack.onVideosRetrieved(videos);
            }
        });
    }
}
