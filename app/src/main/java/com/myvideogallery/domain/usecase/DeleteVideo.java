package com.myvideogallery.domain.usecase;

import com.myvideogallery.domain.DefaultSubscriber;
import com.myvideogallery.domain.Repository;
import com.myvideogallery.domain.UseCase;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Rory_McCormack on 2/08/2016.
 */
public class DeleteVideo extends UseCase {

    public interface CallBack extends BaseCallBack {

        void onVideoDeleted();
    }

    private final Repository repository;

    @Inject
    public DeleteVideo(Repository repository) {
        this.repository = repository;
    }

    public void execute(final CallBack callBack, String videoId) {
        Observable<Boolean> observable = repository.deleteVideoFile(videoId);

        subscribeOnIO(observable, new DefaultSubscriber<Boolean>(callBack) {
            @Override
            public void onNext(Boolean aBoolean) {
                super.onNext(aBoolean);
                callBack.onVideoDeleted();
            }
        });
    }
}
