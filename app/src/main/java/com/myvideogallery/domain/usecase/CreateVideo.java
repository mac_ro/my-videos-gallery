package com.myvideogallery.domain.usecase;

import com.myvideogallery.domain.DefaultSubscriber;
import com.myvideogallery.domain.Repository;
import com.myvideogallery.domain.UseCase;
import com.myvideogallery.domain.model.Video;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Rory_McCormack on 2/08/2016.
 */
public class CreateVideo extends UseCase {

    public interface CallBack extends BaseCallBack {

        void onVideoFileCreated(Video video);

        void onCreateVideoError();

    }

    private final Repository repository;

    @Inject
    public CreateVideo(Repository repository) {
        this.repository = repository;
    }

    public void execute(final CallBack callBack, String path, long duration, long timestamp, long runningTime) {

        if (callBack == null) {
            return;
        }

        Video video = Video.builder()
                .path(path).duration(duration).timeStamp(timestamp).deviceRunningTime(runningTime)
                .build();

        Observable<Video> observable = repository.createVideoFile(video);

        subscribeOnIO(observable, new DefaultSubscriber<Video>(callBack) {

            @Override
            public void onError(Throwable e) {
                callBack.onCreateVideoError();
            }

            @Override
            public void onNext(Video video) {
                callBack.onVideoFileCreated(video);
            }
        });

    }
}
