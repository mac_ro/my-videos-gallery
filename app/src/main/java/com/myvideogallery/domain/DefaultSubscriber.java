package com.myvideogallery.domain;

/**
 * Created by Rory_McCormack on 2/08/2016.
 */
public class DefaultSubscriber<T> extends rx.Subscriber<T> {

    private final UseCase.BaseCallBack callBack;

    protected DefaultSubscriber(UseCase.BaseCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public void onCompleted() {
        // NO IMPLEMENTATION BY DEFAULT

    }

    @Override
    public void onError(Throwable e) {
        // TODO - filter errors for more specific error handling

        callBack.onError();
    }

    @Override
    public void onNext(T t) {
        // NO IMPLEMENTATION BY DEFAULT
    }
}
