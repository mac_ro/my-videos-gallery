package com.myvideogallery.domain.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import auto.parcelgson.AutoParcelGson;


/**
 * Created by Rory_McCormack on 2/08/2016.
 */
@AutoParcelGson
public abstract class Video {

    private static final String TIMESTAMP_FORMAT = "dd MMM yyyy, HH:mm";
    private static final String DURATION_FORMAT = "mm:ss";
    private static final String DEVICE_FORMAT = "HH:mm:ss";


    public abstract String getPath();
    public abstract long getDuration();
    public abstract long getTimeStamp();
    public abstract long getDeviceRunningTime();


    public static Builder builder() {
        return new AutoParcelGson_Video.Builder();
    }

    @AutoParcelGson.Builder
    public abstract static class Builder {
        public abstract Builder path(String path);
        public abstract Builder duration(long duration);
        public abstract Builder timeStamp(long timeStamp);
        public abstract Builder deviceRunningTime(long runningTime);
        public abstract Video build();
    }

    public String getId() {
        return String.valueOf(getTimeStamp());
    }


    public String getTimestampString(Locale locale) {
        long time = getTimeStamp();
        return new SimpleDateFormat(TIMESTAMP_FORMAT, locale).format(new Date(time));
    }

    public String getDurationString(Locale locale) {
        long duration = getDuration();
        return new SimpleDateFormat(DURATION_FORMAT, locale).format(new Date(duration));
    }

    public String getDeviceRunningString(Locale locale) {
        long runningTime = getDeviceRunningTime();
        return new SimpleDateFormat(DEVICE_FORMAT, locale).format(new Date(runningTime));
    }

}
