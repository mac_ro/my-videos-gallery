package com.myvideogallery.domain.thread;

import rx.Scheduler;

/**
 * Created by Rory_McCormack on 2/08/2016.
 */
public interface PostExecutionThread {

    Scheduler getScheduler();
}
